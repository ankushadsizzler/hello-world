package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("hello")
public class HelloWorldController {
	
	@RequestMapping 
	String helloWorld(final Model model){
		model.addAttribute("message","Hello There good people of Earth");
		return "helloworld";
	}
}
